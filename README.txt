PROYECTO 4; 30/10/2019


El proyecto #4 de la materia de Diseño y Análisis de Algoritmos consistió en la construcción 
aleatoria de grafos bajo cuatro diferentes métodos (Erdös y Rényi, Gilbert, Geográfico simple, 
y Barabási-Albert); en donde se asignaban pesos aleatorios a cada arista de dichos grafos, para 
posteriormente encontrar el árbol de expansión mínima a través de los métodos de Prim, Kruskal-Directo
y Kruskal-Inverso. 
Dicho proyecto fue desarrollado en Java, através de NetBeans IDE.


A continuación se describirá de manera concisa el procedimiento a realizar para la generación
de cualquier tipo de grafo y su posterior árbol de expansión mínima:

1.- Abrir en cualquier editor de Java (el proyecto se realizó en NetBeans IDE) el proyecto 
titulado "Proyecto4".
2.- Dentro del proyecto podrán ser visualizadas tres clases: Grafos, Aristas, Nodos. Abrir la
clase "Grafos".
3.- En la clase "Grafos" se encuentra desarrollado el algoritmo pertinente para la generación de
cualquier tipo de grafo (Erdös y Rényi, Gilbert, Geográfico simple, y Barabási-Albert). En la parte
final se puede observar el método "main", donde se da la instrucción de construir el grafo deseado
y colocarle un nombre al archivo .gv.
4.- Una vez generado el grafo dentro de un arvhivo .gv, podrá ser observado de manera gráfica a 
través del software Gephi, en donde se abre el archivo correspondiente y el grafo puede ser 
visualizado.
5.- Al construir cualquier tipo de grafo aleatorio, se le aplican los 3 métodos anteriormente mencionados
(Prim, Kruskal-Directo y Kruskal-Inverso) para encontar el árbol de expansión mínima. Lo anterior generando
previamente los pesos aleatorios a las aristas del grafo (mediante el método de de EdgeValues). Una vez 
generado el árbol de expasión mínima a través de cada método, se imprime el valor total de peso para dicho
árbol.
6.- Al final se obtendrán cuatro archivos .gv que podrán ser visualizados en el software Gephi como
en el proyecto anterior.

- Viridiana Rodríguez González 
package grafo;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Stack;

public class Grafos {

    private HashMap<Integer, Nodos> Nodos;
    private HashMap<Integer, Aristas> Aristas;

    public Grafos(HashMap<Integer, Nodos> N, HashMap<Integer, Aristas> A) {
        this.Aristas = new HashMap();
        for (int i = 0; i < A.size(); i++) {
            this.Aristas.put(i, new Aristas(A.get(i)));
        }
        this.Nodos = new HashMap();
        for (int i = 0; i < N.size(); i++) {
            this.Nodos.put(i, new Nodos(N.get(i)));
        }
    }

    public Grafos() {
        this.Aristas = new HashMap();
        this.Nodos = new HashMap();
    }

    public Grafos(Grafos k) {
        this.Aristas = new HashMap();
        for (int i = 0; i < k.getAristas().size(); i++) {
            this.Aristas.put(i, new Aristas(k.getAristas().get(i)));
        }
        this.Nodos = new HashMap();
        for (int i = 0; i < k.getNodos().size(); i++) {
            this.Nodos.put(i, new Nodos(k.getNodos().get(i)));
        }
    }

    public void setNodos(HashMap<Integer, Nodos> w) {
        this.Nodos = w;
    }

    public void setAristas(HashMap<Integer, Aristas> w) {
        this.Aristas = w;
    }

    public HashMap<Integer, Aristas> getAristas() {
        return this.Aristas;
    }

    public HashMap<Integer, Nodos> getNodos() {
        return this.Nodos;
    }

    public void setG(HashMap<Integer, Nodos> a, HashMap<Integer, Aristas> b) {
        this.Nodos = (HashMap) a;
        this.Aristas = (HashMap) b;
    }
    
    public static Grafos ErdosRenyi(int NumNodos, int NumAristas, int dirigido) {
        HashMap<Integer, Nodos> NodoS = new HashMap();
        HashMap<Integer, Aristas> AristaS = new HashMap();

        int AristasHechas;

        for (int i = 0; i < NumNodos; i++) {
            NodoS.put(i, new Nodos(i));
        }

        int x1 = (int) (Math.random() * NumNodos), x2 = (int) (Math.random() * NumNodos);
        AristaS.put(0, new Aristas(NodoS.get(x1).get(), NodoS.get(x2).get(), Math.random()));

        while (x1 == x2 && dirigido == 0) {
            x1 = (int) (Math.random() * NumNodos);
            x2 = (int) (Math.random() * NumNodos);
            AristaS.put(0, new Aristas(NodoS.get(x1).get(), NodoS.get(x2).get(), Math.random()));
        }

        NodoS.get(x1).conectar();
        NodoS.get(x2).conectar();
        if (x1 != x2) {
            NodoS.get(x1).IncGrado(1);
        }
        NodoS.get(x2).IncGrado(1);

        AristasHechas = 1;
        while (AristasHechas < NumAristas) {
            x1 = (int) (Math.random() * NumNodos);
            x2 = (int) (Math.random() * NumNodos);

            if (x1 != x2 || dirigido == 1) {
                int c1 = 1, cont = 0;
                while (c1 == 1 && cont < AristasHechas) {
                    int a = AristaS.get(cont).getAn1(), b = AristaS.get(cont).getAn2();
                    if ((x1 == a && x2 == b) || (x1 == b && x2 == a)) {
                        c1 = 0;
                    }
                    cont++;
                }
                if (c1 == 1) {
                    AristaS.put(AristasHechas, new Aristas(NodoS.get(x1).get(), NodoS.get(x2).get(), Math.random()));
                    NodoS.get(x1).conectar();
                    NodoS.get(x2).conectar();
                    if (x1 != x2) {
                        NodoS.get(x1).IncGrado(1);
                    }
                    NodoS.get(x2).IncGrado(1);
                    AristasHechas++;
                }
            }
        }

       
        Grafos G = new Grafos(NodoS, AristaS);
        return G;

    }
    
    public static Grafos Geografico(int NumNodos, double distancia, int dirigido) {
        HashMap<Integer, Nodos> NodoS = new HashMap();
        HashMap<Integer, Aristas> AristaS = new HashMap();
        int NumAristas = 0;

        for (int i = 0; i < NumNodos; i++) {
            NodoS.put(i, new Nodos(i, Math.random(), Math.random()));
        }

        for (int i = 0; i < NumNodos; i++) {
            for (int j = i; j < NumNodos; j++) {
                if (j != i || dirigido == 1) {
                    double dis = Math.sqrt(Math.pow(NodoS.get(j).getX() - NodoS.get(i).getX(), 2)
                            + Math.pow(NodoS.get(j).getY() - NodoS.get(i).getY(), 2));
                    if (dis <= distancia) {
                        AristaS.put(NumAristas, new Aristas(NodoS.get(i).get(), NodoS.get(j).get()));
                        NodoS.get(i).IncGrado(1);
                        NodoS.get(i).conectar();
                        if (j != i) {
                            NodoS.get(j).IncGrado(1);
                            NodoS.get(j).conectar();
                        }
                        NumAristas++;
                    }
                }
            }
        }
        Grafos G = new Grafos(NodoS, AristaS);
        return G;
    }

    public static Grafos Gilbert(int NumNodos, double probabilidad, int dirigido) {
        HashMap<Integer, Nodos> NodoS = new HashMap();
        HashMap<Integer, Aristas> AristaS = new HashMap();

        int NumAristas = 0;

        for (int i = 0; i < NumNodos; i++) {
            NodoS.put(i, new Nodos(i));
        }

        for (int i = 0; i < NumNodos; i++) {
            for (int j = i; j < NumNodos; j++) {
                if (j != i || dirigido == 1) {
                    if (Math.random() <= probabilidad) {
                        AristaS.put(NumAristas, new Aristas(NodoS.get(i).get(), NodoS.get(j).get()));
                        NodoS.get(i).conectar();
                        NodoS.get(j).conectar();
                        if (i != j) {
                            NodoS.get(i).IncGrado(1);
                        }
                        NodoS.get(j).IncGrado(1);
                        NumAristas++;
                    }
                }
            }
        }
        Grafos G = new Grafos(NodoS, AristaS);
        return G;
    }

    public static Grafos Barabasi(int NumNodos, double d, int dirigido) {
        HashMap<Integer, Nodos> NodoS = new HashMap();
        HashMap<Integer, Aristas> AristaS = new HashMap();

        int NumAristas = 0;

        for (int i = 0; i < NumNodos; i++) {
            NodoS.put(i, new Nodos(i));
        }

        for (int i = 0; i < NumNodos; i++) {
            int j = 0;
            while (j <= i && NodoS.get(i).getGrado() <= d) {
                if (j != i || dirigido == 1) {
                    if (Math.random() <= 1 - NodoS.get(j).getGrado() / d) {
                        AristaS.put(NumAristas, new Aristas(NodoS.get(i).get(), NodoS.get(j).get()));
                        NodoS.get(i).IncGrado(1);
                        NodoS.get(i).conectar();
                        if (j != i) {
                            NodoS.get(j).IncGrado(1);
                            NodoS.get(j).conectar();
                        }
                        NumAristas++;
                    }
                }
                j++;
            }
        }
        Grafos G = new Grafos(NodoS, AristaS);
        return G;
    }

    public static void construir(String nombre, Grafos g) {
        FileWriter fichero = null;
        PrintWriter pw = null;

        //System.out.println(g.getNodos().size());

        try {
            fichero = new FileWriter(nombre + ".gv");
            pw = new PrintWriter(fichero);
            pw.println("graph 666{"); //Por Gephi
            for (int i = 0; i < g.getNodos().size(); i++) {
                pw.println(g.getNodos().get(i).get() + "  " + "[Label = \"" + g.getNodos().get(i).get() + " (" + String.format("%.2f", g.getNodos().get(i).getwin()) + ")\"]");
            }
            pw.println();
            for (int i = 0; i < g.getAristas().size(); i++) {
                pw.println(g.getAristas().get(i).getAn1() + "--" + g.getAristas().get(i).getAn2() + "  " + "[Label = \"" + String.format("%.2f", g.getAristas().get(i).getP()) + "\"]");
            }
            pw.println("}");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
    
    public static Grafos BFS(Grafos GN, Nodos node) {
        Grafos Gr = new Grafos(GN.getNodos(), GN.getAristas());
        HashMap<Integer, HashMap> Col = new HashMap(); 
        HashMap<Integer, Nodos> Col_1 = new HashMap();   // Nodos
        HashMap<Integer, Nodos> Col_2 = new HashMap();   
        HashMap<Integer, Nodos> Col_3 = new HashMap();       
        HashMap<Integer, Aristas> Col_A = new HashMap(); // Aristas
        int nCol = 0, nC = 0,q = 0;
        Gr.getNodos().get(node.get()).setfal(true);
        Col_1.put(0, Gr.getNodos().get(node.get()));
        Col.put(nCol, (HashMap) Col_1.clone());
        Col_3.put(q, Gr.getNodos().get(node.get()));
        while (Col_1.isEmpty() == false) {
            Col_2.clear();
            nC = 0;
            for (int i = 0; i < Col_1.size(); i++) {
                for (int j = 0; j < Gr.getAristas().size(); j++) {
                    if (Col_1.get(i).get() == Gr.getAristas().get(j).getAn1() && Gr.getNodos().get(Gr.getAristas().get(j).getAn2()).getfal() == false) {
                        Gr.getNodos().get(Gr.getAristas().get(j).getAn2()).setfal(true);
                        Col_2.put(nC, Gr.getNodos().get(Gr.getAristas().get(j).getAn2()));
                        nC++;
                        Col_A.put(q, Gr.getAristas().get(j));
                        q++;
                        Col_3.put(q, Gr.getNodos().get(Gr.getAristas().get(j).getAn2()));
                    }
                    if (Col_1.get(i).get() == Gr.getAristas().get(j).getAn2() && Gr.getNodos().get(Gr.getAristas().get(j).getAn1()).getfal() == false) {
                        Gr.getNodos().get(Gr.getAristas().get(j).getAn1()).setfal(true);
                        Col_2.put(nC, Gr.getNodos().get(Gr.getAristas().get(j).getAn1()));
                        nC++;
                        Col_A.put(q, Gr.getAristas().get(j));
                        q++;
                        Col_3.put(q, Gr.getNodos().get(Gr.getAristas().get(j).getAn1()));
                    }
                }
            }
            nCol++;
            Col_1 = (HashMap) Col_2.clone();
            Col.put(nCol, (HashMap) Col_2.clone());
        }
        Grafos ArBFS = new Grafos();
        ArBFS.setG(Col_3, Col_A);
        return ArBFS;
    }
    
    public static Grafos DFS_R(Grafos GN, Nodos node) {
        Grafos ArDFS_R = new Grafos();
        Grafos ArDFSR_2;
        boolean Ar[][] = new boolean[GN.getNodos().size()][GN.getNodos().size()];
        for (int i = 0; i < GN.getAristas().size(); i++) {
            Ar[GN.getAristas().get(i).getAn1()][GN.getAristas().get(i).getAn2()] = true;
            Ar[GN.getAristas().get(i).getAn2()][GN.getAristas().get(i).getAn1()] = true;
        }
        GN.getNodos().get(node.get()).setfal(true);
        ArDFS_R.getNodos().put(0, new Nodos(GN.getNodos().get(node.get())));
        for (int i = 0; i < GN.getNodos().size(); i++) {
            if (Ar[node.get()][i] == true && GN.getNodos().get(i).getfal() == false) {
                ArDFSR_2 = DFS_R(GN, GN.getNodos().get(i));
                int n4 = ArDFS_R.getNodos().size();
                for (int j = 0; j < ArDFSR_2.getNodos().size(); j++) {
                    ArDFS_R.getNodos().put(n4 + j, ArDFSR_2.getNodos().get(j));
                }
                ArDFS_R.getAristas().put(ArDFS_R.getAristas().size(), new Aristas(node.get(), i));
                n4 = ArDFS_R.getAristas().size();
                if (ArDFSR_2.getAristas().isEmpty() != true) {
                    for (int j = 0; j < ArDFSR_2.getAristas().size(); j++) {
                        ArDFS_R.getAristas().put(n4 + j, ArDFSR_2.getAristas().get(j));
                    }
                }
            }
        }
        return ArDFS_R;
    }
    
    public static Grafos DFS_I(Grafos GN, Nodos node) {
        Grafos Gr2 = new Grafos(GN.getNodos(), GN.getAristas());
        Grafos ArDFS_I = new Grafos();
        int p=0, n2 = 0;
        boolean n3;
        boolean Ar[][] = new boolean[Gr2.getNodos().size()][Gr2.getNodos().size()];
        for (int i = 0; i < Gr2.getAristas().size(); i++) {
            if (Gr2.getAristas().get(i).getfal() == false) {
                Ar[Gr2.getAristas().get(i).getAn1()][Gr2.getAristas().get(i).getAn2()]=true;
                Ar[Gr2.getAristas().get(i).getAn2()][Gr2.getAristas().get(i).getAn1()]=true;
            }
        }
        Stack<Integer> heap = new Stack<>();
        heap.push(Gr2.getNodos().get(node.get()).get());
        Gr2.getNodos().get(node.get()).setfal(true);
        ArDFS_I.getNodos().put(n2, new Nodos(Gr2.getNodos().get(node.get())));
        while (heap.isEmpty() == false) {
            p = heap.peek();
            n3 = false;
            for (int j = 0; j < Gr2.getNodos().size(); j++) {
                if (Ar[p][j] == true && Gr2.getNodos().get(j).getfal() == false) {
                    Gr2.getNodos().get(j).setfal(true);
                    ArDFS_I.getAristas().put(n2, new Aristas(p, j));
                    n2++;
                    ArDFS_I.getNodos().put(n2, new Nodos(Gr2.getNodos().get(j)));
                    heap.push(j);
                    n3 = true;
                    j = Gr2.getNodos().size();
                }
                if (j == Gr2.getNodos().size() - 1 && n3 == false) {
                    heap.pop();
                }
            }
        }
        return ArDFS_I;
    }
    
    // Método para asignar valores aleatorios de coste a los aristas, entre 
    // un valor mínimo y un máximo:
     public Grafos EdgeValues(double minimo, double maximo) {
        int AristasN = this.Aristas.size();
        for (int i = 0; i < AristasN; i++) {
            this.Aristas.get(i).setP(Math.random() * (maximo - minimo) + minimo);
        }
        Grafos G = new Grafos(this.Nodos, this.Aristas);
        return G;
    }
    
    public Grafos Dijkstra(Nodos raiz) {
        for (int k = 0; k < this.Nodos.size(); k++) {
            this.Nodos.get(k).setwi(Double.POSITIVE_INFINITY); // Se inicializan los nodos en infinito
        }
        double MatrixA[][] = new double[this.Nodos.size()][this.Nodos.size()];//MatrixA es la matriz de adyacencia con los costes de aristas

        //Llenar la matriz de adyacencia con infinitos:
        for (int i = 0; i < this.Nodos.size(); i++) {
            for (int j = 0; j < this.Nodos.size(); j++) {
               MatrixA[i][j] = Double.POSITIVE_INFINITY;
            }
        }
        //Coloca el coste de cada arista dentro de la entrada que le corresponde en la matriz
        for (int k = 0; k < this.Aristas.size(); k++) {
            MatrixA[this.Aristas.get(k).getAn1()][this.Aristas.get(k).getAn2()] = this.Aristas.get(k).getP();
            MatrixA[this.Aristas.get(k).getAn2()][this.Aristas.get(k).getAn1()] = this.Aristas.get(k).getP();
        }
        ArrayList<Integer> NodosVisitados = new ArrayList<Integer>();//Declaracion del conjunto de nodos que se van visitando
        HashMap<Integer, Aristas> AZ = new HashMap(); //Vector que almacena las aristas para el árbol resultante
        int numA = 0;
        //Se establece como primer elemento en el conjunto de nodos visitados el nodo
        //que entro como parametro al metodo para ser la raiz del árbol
        NodosVisitados.add(this.Nodos.get(raiz.get()).get());
        this.Nodos.get(raiz.get()).setfal(true);
        this.Nodos.get(raiz.get()).setwi(0);
        //IMPORTANTE, la variable "aux" guarda el valor del recorrido de menor coste dentro de los nodos visitados, es decir,
        //guarda el valor de la distancia menor (el camino más corto encontrado)
        //Las variables "a" y "b" guardan el nodo que se encontró y lo agregan a la lista 
        double aux = 0; //inicializando variable
        int a = 0, b = 0;
        boolean parar = true;
        //====System.out.println("grafo.Grafo.Dijkstra()");

        while (aux != Double.POSITIVE_INFINITY) {
            aux = Double.POSITIVE_INFINITY;
            //====System.out.println("================" + aux);
            for (int i = 0; i < NodosVisitados.size(); i++) {
                //====System.out.println(NodosEncontrados.get(i));
                for (int j = 0; j < this.Nodos.size(); j++) {
                    //====System.out.print("");
                    if (this.Nodos.get(j).getfal() != true && MatrixA[NodosVisitados.get(i)][j] != Double.POSITIVE_INFINITY) {
                        //====System.out.println("    "+j+" -> "+(this.Nodos.get(NodosEncontrados.get(i)).getW()+MA[NodosEncontrados.get(i)][j]));
                        if ((this.Nodos.get(NodosVisitados.get(i)).getwin() + MatrixA[NodosVisitados.get(i)][j]) < aux) {
                            this.Nodos.get(j).setwi(this.Nodos.get(NodosVisitados.get(i)).getwin() + MatrixA[NodosVisitados.get(i)][j]);
                            aux = this.Nodos.get(j).getwin();
                            a = NodosVisitados.get(i);
                            b = j;
                            //System.out.println("        " + aux + "   " + a+"-->"+b);
                        }
                    }
                }
            }
            if (aux != Double.POSITIVE_INFINITY) {
                this.Nodos.get(b).setfal(true);
                NodosVisitados.add(b);
                System.out.println("          " + aux + "   " + a +"-->"+ b);
                AZ.put(numA, new Aristas(a, b, MatrixA[a][b]));
                numA++;
                }

        }

        HashMap<Integer, Nodos> NZ = new HashMap();
        for (int i = 0; i < NodosVisitados.size(); i++) {
            NZ.put(i, new Nodos(this.Nodos.get(NodosVisitados.get(i))));
        }
        Grafos G1 = new Grafos(NZ, AZ);
        return G1;
        

    }
    
    public Grafos Prim() {
        Grafos G = new Grafos(this.Nodos, this.Aristas);
        Aristas MatrixA[][] = new Aristas[G.getNodos().size()][G.getNodos().size()]; // Al igual que en el método Dijkstra, le implementa la matriz de abyacencia
        //Formación de MatrixA
        for (int i = 0; i < G.getAristas().size(); i++) {
            MatrixA[G.getAristas().get(i).getAn1()][G.getAristas().get(i).getAn2()] = G.getAristas().get(i);
            MatrixA[G.getAristas().get(i).getAn2()][G.getAristas().get(i).getAn1()] = G.getAristas().get(i);
        }
        ArrayList<Integer> NodosVisitados = new ArrayList<Integer>(); // Conjunto de nodos visitados
        // Cada que un nodo es encontrado su valor ID es almacenado en el conjunto 
        HashMap<Integer, Aristas> P_aristas = new HashMap(); // Se declara P_aristas como el vector que almacena las aristas para el árbol resultante 
        int nodo_n = 0; // Iniciamos con el nodo 0 
        NodosVisitados.add(G.getNodos().get(0).get());
        G.getNodos().get(0).setfal(true);
        PriorityQueue<Aristas> c_priori = new PriorityQueue<>();
        double d = 0;
        for (int m = 0; m < G.getAristas().size(); m++) {
            for (int k = 0; k < NodosVisitados.size(); k++) {
                for (int j = 0; j < G.getNodos().size(); j++) {
                    if (MatrixA[NodosVisitados.get(k)][j] != null && G.getNodos().get(j).getfal() == false) {
                        c_priori.add(MatrixA[NodosVisitados.get(k)][j]);
                    }
                }
            }
            if (c_priori.isEmpty() == false) {
                if (NodosVisitados.contains(c_priori.peek().getAn1())) {
                    G.getNodos().get(c_priori.peek().getAn2()).setfal(true);
                    NodosVisitados.add(c_priori.peek().getAn2());
                    P_aristas.put(nodo_n, MatrixA[c_priori.peek().getAn1()][c_priori.peek().getAn2()]);
                    nodo_n++;
                    d = d + c_priori.peek().getP();
                    c_priori.clear();
                } else {
                    G.getNodos().get(c_priori.peek().getAn1()).setfal(true);
                    NodosVisitados.add(c_priori.peek().getAn1());
                    P_aristas.put(nodo_n, MatrixA[c_priori.peek().getAn1()][c_priori.peek().getAn2()]);
                    nodo_n++;
                    d = d + c_priori.peek().getP();
                    c_priori.clear();
                }
            }
            if (P_aristas.size() == G.getNodos().size() - 1) {
                break;
            }
        }
        System.out.println("Valor total del peso del árbol MST, aplicando Prim =  "+d);
        Grafos P = new Grafos(G.getNodos(), P_aristas);
        return P;
    }
    
    public Grafos Kruskal_D() {
        Grafos G = new Grafos(this.Nodos, this.Aristas);
        // Almacena las aristas del grafo generado con pesos aleatorios
        // Utilización de una cola de prioridad para aristas con peso mínimo
        PriorityQueue<Aristas> c_priori = new PriorityQueue<>();
        for (int i = 0; i < G.getAristas().size(); i++) {
            c_priori.add(G.getAristas().get(i));
        }
        HashMap<Integer, Aristas> MST = new HashMap(); // Aquí se guardan las aristas del árbol MST
        HashMap<Integer, Nodos> KG = new HashMap();
        ArrayList<Integer> NodosVisitados = new ArrayList<Integer>(); // Se almacena el ID de cada nodo para ir construyendo el árbol
        int nA = 0;
        int nNodo = 0;
        double d = 0;
        for (int i = 0; i < G.getAristas().size(); i++) {//El for corre sobre toda la coleccion de aristas
            if (NodosVisitados.contains(c_priori.peek().getAn1()) && NodosVisitados.contains(c_priori.peek().getAn2())) {
                Grafos aux_1 = new Grafos(G.getNodos(), MST); // variable auxiliar para verificar si se forma un ciclo
                Grafos aux_2 = new Grafos();
                aux_2 = DFS_I(aux_1, aux_1.getNodos().get(c_priori.peek().getAn1()));
                int bandera = 0;
                for (int k = 0; k < aux_2.getNodos().size(); k++) {
                    if (aux_2.getNodos().get(k).get() == c_priori.peek().getAn2()) {
                        bandera = 1;
                    }
                }
                if (bandera == 0) {
                    d = d + c_priori.peek().getP();
                    MST.put(nA, c_priori.poll());
                    nA++;
                } else {
                    c_priori.poll();
                }
            } else {
                if (NodosVisitados.contains(c_priori.peek().getAn1()) == false) {
                    NodosVisitados.add(c_priori.peek().getAn1());
                }
                if (NodosVisitados.contains(c_priori.peek().getAn2()) == false) {
                    NodosVisitados.add(c_priori.peek().getAn2());
                }
                d = d + c_priori.peek().getP();
                MST.put(nA, c_priori.poll());
                nA++;
            }
            if (MST.size() == G.getNodos().size() - 1) {
                i = G.getAristas().size();
            }
        }
        for (int l = 0; l < NodosVisitados.size(); l++) {
            KG.put(l, G.getNodos().get(NodosVisitados.get(l)));
        }
        System.out.println("Valor total del peso del árbol MST, aplicando Kruskal_D =  "+d);
        Grafos K_D = new Grafos(KG, MST);
        return K_D;
    }
    
    public Grafos Kruskal_I() {
        Grafos G = new Grafos(this.Nodos, this.Aristas);
        for (int i = 0; i < G.getAristas().size(); i++) // ID para cada arista
            G.getAristas().get(i).setID(i);
        // Cola de prioridad:
        PriorityQueue<Aristas> c_priori = new PriorityQueue<>(Collections.reverseOrder());
        // Ingresar aristas en la cola de prioridad:
        for (int i = 0; i < G.getAristas().size(); i++) 
            c_priori.add(G.getAristas().get(i));
        int nA = 0; // Inicializando 
        HashMap<Integer, Aristas> MST = new HashMap();
        Grafos aux = new Grafos();
        Aristas borrar = new Aristas();
        double min_exp = 0;
        for (int j = 0; j < G.getAristas().size(); j++) {
            borrar = c_priori.poll();
            G.getAristas().get(borrar.getID()).setfal(true);
            aux = DFS_I(G, G.getNodos().get(borrar.getAn1()));
            if (aux.getNodos().size()<G.getNodos().size()){
                MST.put(nA, new Aristas(borrar.getAn1(), borrar.getAn2(), borrar.getP()));
                nA++;
                G.getAristas().get(borrar.getID()).setfal(false);
                min_exp = min_exp + borrar.getP();
            } 
        }
        System.out.println("Valor total del peso del árbol MST, aplicando Kruskal_I =  "+min_exp);
        
        Grafos KI = new Grafos(G.getNodos(), MST);
        return KI;
    }
    
    public static void main(String[] args) {
              
        //Imprimir los 4 tipos de grafos aleatorios:
        Grafos GN = new Grafos();
        //GN = ErdosRenyi(500,2500,0);
        //GN = Geografico(500,.6,0);
        //GN = Gilbert(500,.2,0);
        GN = Barabasi(100,200,0); 
        construir("Barabasi100_original",GN);
        
        // Imprimir el árbol de Dijkstra:
        //GN.EdgeValues(8,150);
        //GN = GN.Dijkstra(GN.getNodos().get(0)); // El nodo 0 es el nodo inicio o raíz
        
        //Imprimir la BFS y DFS:
        //Grafos GBFS = new Grafos(BFS(GN,GN.getNodos().get(0)));
        //Grafos GDFSI = new Grafos(DFS_I(GN,GN.getNodos().get(0)));
        //Grafos GDFSR = new Grafos(DFS_R(GN,GN.getNodos().get(0)));
      
        // Construir el archivo del árbol de Dijkstra:
        //construir("E100_Dijkstra",GN);
        
        // Construir los archivos de BFS y DFS:
        //construir("grafoB3_BFS",GBFS);
        //construir("grafoB3_DFS-I",GDFSI);
        //construir("grafoB3_DFS-R",GDFSR);
        
        //Prim y Kruskal
        GN.EdgeValues(1,1000);
        Grafos GP = new Grafos();
        GP = GN.Prim();
        construir("Prim", GP);
        Grafos GKD = new Grafos();
        GKD = GN.Kruskal_D();
        construir("Kruskal_Directo", GKD);
        Grafos GKI = new Grafos();
        GKI = GN.Kruskal_I();
        construir("Kruskal_Inverso", GKI);
    }
}
